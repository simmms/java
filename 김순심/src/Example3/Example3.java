package Example3;

import java.util.Scanner;

public class Example3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("이름입력 >");
		String name = sc.nextLine();
		System.out.println("비밀번호입력 >");
		String password = sc.nextLine();

		if (name.length() >= 3 && password.length() >= 8) {
			System.out.println("결과 : 사용가능");
		} else {
			System.out.println("결과  : 사용불가능");
		}
	}
}
