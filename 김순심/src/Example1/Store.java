package Example1;

public interface Store {
	
	public static final double CARD_PAYMENT_RATIO = 0.01;
	public static final double CASH_PATMENT_RATIO = 0.03;
	
	public abstract int card(int price);
	public abstract int cash(int price);
	public abstract void showInfo();
	
}
