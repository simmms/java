package Example1;

public class Clothes implements Store{
	
	
	private String storeName;
	private double storeRatio;
	
	public Clothes(String storeName,double storeRatio) {
		this.storeName = storeName;
		this.storeRatio = storeRatio;
		
	}
	
	

	@Override
	public int card(int price) {
		int result = price - (int)(price * (storeRatio + CARD_PAYMENT_RATIO));
		return result;
	}



	@Override
	public int cash(int price) {
		int result = price - (int)(price * (storeRatio + CASH_PATMENT_RATIO ));
		return result;
	}



	@Override
	
	public void showInfo() {
		System.out.println("######의류매장입니다.");
		System.out.println("카드 결제 금액 : " + (storeRatio + CARD_PAYMENT_RATIO));
		System.out.println("현금 결제 금액 : " + (storeRatio + CASH_PATMENT_RATIO));
	}
	
	
		
}
